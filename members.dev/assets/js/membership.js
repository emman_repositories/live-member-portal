var compare_date = '';
var cancel_message = '';
var cancel_dialog = '';

$(document).ready(function(){
    $(".alert").fadeTo(5000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });
    $('div#content > .loadingDiv').hide();
});
var select_options = '';
$(document).on('click', '.pause-membership', function(){
    select_options = '<select name="pause_membership" class="form-control pause-membership-options">';
    select_options += '<option value="" selected disabled>Select one.</option>';
    select_options += '<option value="1">1 month</option>';
    select_options += '<option value="2">2 months</option>';
    select_options += '<option value="3">3 months</option>';
    select_options += '</select>';
    var pause_bootbox = bootbox.confirm({
        title: "Pause Membership",
        message: '<p class="text-left">How long do you want to pause for?</p>' + select_options,
        buttons: {
            confirm: {
                label: 'OK',
                className: 'btn-brunchwork pause-membership-confirm'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-brunchwork pause-membership-cancel'
            }
        },
        callback: function (result) {
            
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    var selected_pause = $('select.pause-membership-options').val();
                    $.ajax({
                        url: window.base_url + 'member/pause_subscription',
                        type: "GET",
                        data: {no_of_month: selected_pause},
                        dataType:'JSON',
                        success: function(response) {
                            console.log(response);
                            if(response.success == true){
                                $('.modal, .bootbox-confirm').modal('hide');
                                $('.modal, .bootbox-confirm').on('hidden', function () {
                                    // write your code
                                    $('.bootbox-confirm > .loadingDiv').show();
                                });
                                window.location.href = window.base_url + 'settings?no_of_month=' + selected_pause;
                            }
                            
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
    pause_bootbox.init(function(){
        $('.pause-membership-confirm').attr('disabled', 'disabled');
    });
});

$(document).on('change', '.pause-membership-options', function(){
    $('.pause-membership-confirm').removeAttr('disabled');
});

$(document).on('click', '.unpause-membership', function(){
    bootbox.confirm({
        title: "Continue with Membership",
        message: '<p class="text-left">Are you sure you want to continue your membership?</p>',
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-brunchwork'
            },
            cancel: {
                label: 'No',
                className: 'btn-brunchwork'
            }
        },
        callback: function (result) {
            console.log(result);
            if(result == null || result == false) {
                $(this).modal('hide');
            } else {
                if(result == true){
                    $.ajax({
                        url: window.base_url + 'member/unpause_subscription',
                        type: "GET",
                        dataType:'JSON',
                        success: function(response) {
                            console.log(response);
                            if(response.success == true){
                                $('.modal, .bootbox-confirm').modal('hide');
                                $('.modal, .bootbox-confirm').on('hidden', function () {
                                    // write your code
                                    $('.bootbox-confirm > .loadingDiv').show();
                                });
                                  
                                window.location.href = window.base_url + 'settings?unpause=success';
                            }
                            
                        },
                        error: function (MLHttpRequest, textStatus, errorThrown) {
                          console.log("There was an error: " + errorThrown);
                        }
                    });
                }
            }
        }
    });
});

function dateValidation() {
    var b = new Date(first_bill_date); //your input date here

    var d = new Date(); // today date
    d.setMonth(d.getMonth() - 6);  //subtract 6 months from current date 

    if (b > d) {
        //alert("date is less than 6 month");
        compare_date = 'less';
    } else {
        //alert("date is older than 6 month");
        compare_date = 'more';
    }
    return compare_date;
}

var cancel_buttons = '';
var bootbox_type = '';
$(document).on('click', '.cancel-membership', function(){
    var pause_button = '';
    var pause_button_class = '';
    var pause_case = '';
    if(is_paused == 1){
        pause_button  = 'Unpause Membership';
        pause_button_class = 'unpause-membership btn-brunchwork pull-left';
        pause_case = 'unpause';
    } else {
        pause_button  = 'Pause Membership';
        pause_button_class = 'pause-membership btn-brunchwork pull-left';
        pause_case = 'pause';
    }
    var check_date = dateValidation();
    if(check_date == 'less'){
        cancel_message = '<p>Membership is a 6-month commitment. You can cancel at the end of 6 months.</p>';
        bootbox_type = 'alert';
    } else if(check_date == 'more'){
        show_button = true;
        cancel_message = '<p>Are you sure you want to cancel? You can pause your account and stay grandfathered into your current membership rate.</p>';
        cancel_buttons = {
            pause: {
                label: pause_button,
                className: pause_button_class,
                callback: function(){
                    if(pause_case == 'pause'){
                        select_options = '<select name="pause_membership" class="form-control pause-membership-options">';
                        select_options += '<option value="" selected disabled>Select one.</option>';
                        select_options += '<option value="1">1 month</option>';
                        select_options += '<option value="2">2 months</option>';
                        select_options += '<option value="3">3 months</option>';
                        select_options += '</select>';
                        var pause_bootbox = bootbox.confirm({
                            title: "Pause Membership",
                            message: '<p class="text-left">How long do you want to pause for?</p>' + select_options,
                            buttons: {
                                confirm: {
                                    label: 'OK',
                                    className: 'btn-brunchwork pause-membership-confirm'
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-brunchwork pause-membership-cancel'
                                }
                            },
                            callback: function (result) {
                                
                                if(result == null || result == false) {
                                    $(this).modal('hide');
                                } else {
                                    if(result == true){
                                        var selected_pause = $('select.pause-membership-options').val();
                                        $.ajax({
                                            url: window.base_url + 'member/pause_subscription',
                                            type: "GET",
                                            data: {no_of_month: selected_pause},
                                            dataType:'JSON',
                                            success: function(response) {
                                                console.log(response);
                                                if(response.success == true){
                                                    $('.modal, .bootbox-confirm').modal('hide');
                                                    $('.modal, .bootbox-confirm').on('hidden', function () {
                                                        // write your code
                                                        $('.bootbox-confirm > .loadingDiv').show();
                                                    });
                                                    window.location.href = window.base_url + 'settings?no_of_month=' + selected_pause;
                                                }
                                                
                                            },
                                            error: function (MLHttpRequest, textStatus, errorThrown) {
                                            console.log("There was an error: " + errorThrown);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        pause_bootbox.init(function(){
                            $('.pause-membership-confirm').attr('disabled', 'disabled');
                        });
                    } else if(pause_case == 'unpause'){
                        bootbox.confirm({
                            title: "Continue with Membership",
                            message: '<p class="text-left">Are you sure you want to continue your membership?</p>',
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-brunchwork'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-brunchwork'
                                }
                            },
                            callback: function (result) {
                                console.log(result);
                                if(result == null || result == false) {
                                    $(this).modal('hide');
                                } else {
                                    if(result == true){
                                        $.ajax({
                                            url: window.base_url + 'member/unpause_subscription',
                                            type: "GET",
                                            dataType:'JSON',
                                            success: function(response) {
                                                console.log(response);
                                                if(response.success == true){
                                                    $('.modal, .bootbox-confirm').modal('hide');
                                                    $('.modal, .bootbox-confirm').on('hidden', function () {
                                                        // write your code
                                                        $('.bootbox-confirm > .loadingDiv').show();
                                                    });
                                                      
                                                    window.location.href = window.base_url + 'settings?unpause=success';
                                                }
                                                
                                            },
                                            error: function (MLHttpRequest, textStatus, errorThrown) {
                                              console.log("There was an error: " + errorThrown);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }
            },
            confirm: {
                label: 'Cancel Membership',
                className: 'btn-brunchwork cancel_mem',
                callback: function(result){
                    console.log(result);
                    return false;
                    cancel_subscription();
                }

            }
        };
        bootbox_type = 'dialog';
    }
    if(bootbox_type == 'dialog'){
        cancel_dialog = bootbox.dialog({
            title: "Cancel Membership",
            message: cancel_message,
            buttons: cancel_buttons
        });
    
        cancel_dialog.init(function(){
            $('.cancel_membership_ok').css('display', 'none');
            $('.cancel-button').css('display', 'none');
        });
    } else {
        cancel_dialog = bootbox.alert({
            title: "Cancel Membership",
            message: cancel_message,
        });
        cancel_dialog.init(function(){
            $('.modal-footer button').addClass('btn-brunchwork');
        });
    }
});

$(document).on('click', 'a#quick_survey', function(){
    cancel_subscription();
});

function cancel_subscription(){
    $('div#content > .loadingDiv').show();
    window.location.href = window.base_url + 'member/cancel';
    /*
    $.ajax({
        url: window.base_url + 'member/cancel_subscription',
        type: "GET",
        dataType:'JSON',
        success: function(response) {
            console.log(response);
            if(response.success == true){
                cancel_dialog.modal('hide');
                //window.location.href = 'https://goo.gl/forms/xgSrAx7cTuyZ9NDH3';
                window.location.href = window.base_url + 'member/cancel';
            }
            
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
          console.log("There was an error: " + errorThrown);
        }
    });
    */
}