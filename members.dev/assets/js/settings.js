var edit_form = '';

$(document).on('click', '#credit_card_btn', function(){
    var data_save = $('#credit_card_form').serializeArray();
    data_save.push({ name: "cc_token", value: $('input#cc_token').val() });
    data_save.push({ name: "cc_bin", value: $('input#cc_bin').val() });
    data_save.push({ name: "customer_braintree_id", value: $('input#customer_braintree_id').val() });
    if($("#credit_card_form").valid()){
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/submit_credit_card', 
            data: data_save,
            dataType : 'JSON',
            success: function (response) {
                $('div.cc_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    }
});

$(document).ready(function(){
    $("#credit_card_form").validate({
        rules: {
            cardnumber: "required",
            expiry: "required",
            cvc: "required"
        }
    });
    $("#email_form").validate({
        rules: {
            update_email: "required"
        }
    });
    $("#mailing_address_form").validate({
        rules: {
            street: "required",
            extended: "required",
            city: "required",
            state: "required",
            postal: "required",
            country: "required",
        }
    });
});

$(document).on('click', '#email_btn', function(){
    if($("#email_form").valid()){
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/update_email', 
            data: $('#email_form').serializeArray(),
            dataType : 'JSON',
            success: function (response) {
                $('div.update_email_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    }
});

$(document).on('click', 'button#add_address', function(){
    if($("#mailing_address_form").valid()){
        var addr_data = $("#mailing_address_form").serializeArray();
        addr_data.push({ name: "customer_braintree_id", value: $('input#customer_braintree_id').val() });
        addr_data.push({ name: "countryCodeAlpha2", value: $('select#country_options :selected').data('alpha2') });
        addr_data.push({ name: "countryCodeAlpha3", value: $('select#country_options :selected').data('alpha3') });
        addr_data.push({ name: "countryCodeNumeric", value: $('select#country_options :selected').data('numeric') });
        addr_data.push({ name: "countryName", value: $('select#country_options :selected').val() });
        addr_data.push({ name: "address_id", value: $('input#address_id').val() });
        addr_data.push({ name: "csrf_test_name", value: $('input#csrf').val() });

        $.ajax({
            type: "POST", 
            url: window.base_url+'user/update_address', 
            data: addr_data,
            dataType : 'JSON',
            success: function (response) {
                $('div.update_address_alert').html('<div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    }
});

$(document).on('click', 'a.contact-support-form', function(){
    $('.loadingDiv').hide();
    email_to = 'concierge@brunchwork.com';
    $('input#dir_to_email').val(email_to);
    var support_contact = bootbox.dialog({
        className: 'support-form-modal',
        message: '<div id="form-holder"></div>'
    });
    support_contact.init(function(){
        $(this).find('.modal-body').addClass('bg-pink');
    });
    $('.support-form-modal').find('div#form-holder').load(window.base_url + 'member/mdirectory/contact_member', { email: email_to });
    
});

$(document).on('click', 'button.send_dir_email', function(){
    $('.loadingDiv').show();
    if($('input#dir_to_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#contact-memberdir-form").valid()){
        var contact_member_data = $("#contact-memberdir-form").serializeArray();
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/contact_support', 
            data: contact_member_data,
            dataType : 'JSON',
            success: function (response) {
                $('.loadingDiv').hide();
                $('#contact-memberdir-form')[0].reset();
                $('.support-form-modal').find('.bootbox-body').prepend('<div class="twenty-spacer"></div><div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('label.error').css('display', 'none');
    }
});