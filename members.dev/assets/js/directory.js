var member = '';
var member_industry = '';
var member_city = '';
var email_to = '';
var orig_content = '';

function imgError(image, gender, id) {
    image.onerror = "";
//    image.src = "/images/noimage.gif";
    if(gender == 'male') {
        image.src = 'assets/images/default.png';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/default.png');
    } else if(gender == 'female') {
        image.src = 'assets/images/default.png';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/default.png');
    } else {
        image.src = 'assets/images/default.png';
        $('img.member-'+ id).data('linkedin_pic', 'assets/images/default.png');
    }
    return true;
}

function imageExists(url, callback) {
    var img = new Image();
    img.onload = function() { callback(true); };
    img.onerror = function() { callback(false); };
    img.src = url;
}

$(document).ready(function(){
    $('.loadingDiv').hide();
    // Detect pagination click
    $('#pagination').on('click','a',function(e){
        e.preventDefault(); 
        var pageno = $(this).attr('data-ci-pagination-page');
        loadPagination(pageno, member, member_industry, member_city);
    });

    loadPagination(0);
});

$(document).on('click', 'button.memberdir_refresh', function(){
    $('.loadingDiv').show();
    $('select#member_industry').val();
    $('select#member_city').val('');
    member = '';
    member_city = '';
    member_industry = '';
    loadPagination(0);
});

$(document).on('click', 'button.search-memberdir', function(){
    $('.loadingDiv').show();
    member = $('input#member_name').val();
    member_industry = $('select#member_industry').val();
    member_city = $('select#member_city').val();
    loadPagination(0, member, member_industry, member_city);
    $('.loadingDiv').hide();
});

$(document).on('click', 'button.search-membercity', function(){
    $('.loadingDiv').show();
    member_city = $('select#member_city').val();
    member_industry = $('select#member_industry').val();
    if(member_city != ''){
        loadPagination(0, member, member_industry, member_city);
    }
});

$(document).on('click', 'button.search-membertag', function(){
    $('.loadingDiv').show();
    member_industry = $('select#member_industry').val();
    member_city = $('select#member_city').val();
    if(member_industry != ''){
        loadPagination(0, member, member_industry, member_city);
    }
});



// Load pagination
function loadPagination(pagno, member, member_industry, member_city){
    var pagination_url = window.base_url + 'member/mdirectory/loadRecord/'+pagno;

    $.ajax({
        url: pagination_url,
        type: 'get',
        data: {'member_data': member, 'member_industry': member_industry, 'member_city': member_city},
        dataType: 'json',
        success: function(response){
            console.log(response);
            $('.loadingDiv').hide();
            $('#pagination').html(response.pagination);
            createTable(response.result,response.row);
        }
    });
}

// Create table list
function createTable(result,sno){
    sno = Number(sno);
    var ctr = 0
    var tr = '';
    var ctr2 = 0;
    $('div.memberdirectory_holder').empty();
    if(result != ''){
        for(index in result){
            var id = result[index].id;
            var first_name = result[index].first_name;
            var last_name = result[index].last_name;
            var linkedin_pic = result[index].profile_image;
            var occupation = result[index].position;
            var company = result[index].company;
            var industry = result[index].industry;
            var first_bill_date = result[index].first_bill_date;
            var gender = result[index].gender;
            var bio = result[index].bio;
            var email = result[index].email;
            var braintree_customer_id = result[index].braintree_customer_id;
            var city_id = result[index].city_id;
    
            if(!linkedin_pic) {
                /*
                if(result[index].gender == 'male') {
                    linkedin_pic = 'assets/images/male.jpg';
                } else if(result[index].gender == 'female') {
                    linkedin_pic = 'assets/images/female.png';
                } else {
                    linkedin_pic = 'assets/images/random-profile.png';
                }
                */
                linkedin_pic = 'assets/images/default.png';
            }
            if(city_id){
                var city = "";
                if(city_id == 1){
                    city = "NYC";
                }
                if(city_id == 2){
                    city = "SF";
                }
                if(city_id == 3){
                    city = "LA";
                }
            }
            if(!company){
                company = "&nbsp;";
            }
            
            sno+=1;
            if(ctr == 0){
                tr += '<div class="card-deck">';
            }
            tr += '<div class="col-md-4">';
            tr += '<div class="card">';
            tr += '<div class="twenty-spacer"></div>';
            tr += '<div class="text-center"><img class="card-img-top rounded-circle directory_img modal-member member-' + id + '" data-city= "'+ city +'" data-first_name= "'+ first_name +'" data-last_name = "' + last_name + '" data-linkedin_pic = "' + linkedin_pic + '" data-occupation = "' + occupation + '" data-company = "' + company + '" data-industry = "' + industry + '"  data-joined = "' + first_bill_date + '" datadata-member_id = "' + id + '" src="' + linkedin_pic + '" data-bio = "' + bio + '" data-braintree_customer_id = "' + braintree_customer_id + '" data-email = "' + email + '" alt="Card image cap" onerror="imgError(this, '+ gender + ', ' + id + ');"></div>';
            tr += '<div class="card-body text-center">';
            tr += '<h5 class="card-title modal-member member-' + id + '" data-first_name= "'+ first_name +'" data-last_name = "' + last_name + '" data-linkedin_pic = "' + linkedin_pic + '" data-occupation = "' + occupation + '" data-company = "' + company + '" data-industry = "' + industry + '"  data-joined = "' + first_bill_date + '" data-member_id = "' + id + '" data-bio = "' + bio + '" data-braintree_customer_id = "' + braintree_customer_id + '" data-email = "' + email + '">'+ first_name + ' ' + last_name + '</h5>';
            tr += '<h5>'+company+'</h5>'
            tr += '<a class="list-group-item list-group-item-action request-intro-form add-pointer" data-email="'+email+'" data-first_name= "'+ first_name +'" data-last_name = "' + last_name + '">Request Intro</a>'
            tr += '</div>';
            tr += '</div>';
            tr += '</div>';
            if(ctr == 2){
                tr += '</div>';
                tr += '<div class="twenty-spacer"></div>';
    
                ctr = 0;
            } else {
                ctr++;
            }
            ctr2++;
        }
    } else {
        tr = '<p>There are no members matching in the search.';
    }
    $('div.memberdirectory_holder').append(tr);
}

$(document).on('click', '.directory_img', function(){
    $('.loadingDiv').show();
    var member_name = $(this).data('first_name') + ' ' + $(this).data('last_name');
    var member_pic = $(this).data('linkedin_pic'); //$(this).attr('src');
    var member_occupation = $(this).data('occupation');
    var member_company = $(this).data('company');
    var member_industry = $(this).data('industry');
    var member_joined = $(this).data('joined');
    var member_bio = $(this).data('bio');
    var member_id = $(this).data('member_id');
    var member_braintree_customer_id = $(this).data('braintree_customer_id');
    var member_email = $(this).data('email');
    var member_city = $(this).data('city');
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var d = new Date(member_joined);
    var member_month = month[d.getMonth()];
    var member_year = d.getFullYear();
    var member_address = '';
    var intro = '';
    if((member_occupation != '' && member_occupation != null) && (member_company != '' && member_company != null)){
        intro = member_occupation + ' at ' + member_company;
    } else {
        if(member_occupation != '' && member_occupation != null){
            intro = member_occupation;
        } else if(member_company != '' && member_company != null){
            intro = member_company;
        } else {
            intro = '';
            member_company = (member_company == null) ? 'N/A' : member_company; 
        }
    }
    if(member_bio == '' || member_bio == null){
        member_bio = $(this).data('first_name') + ' hasn\'t added a Bio.';
    }
    if(member_occupation == '' || member_occupation == null){
        member_occupation = $(this).data('first_name') + ' hasn\'t added an Occupation.';
    }
    if(member_industry == '' || member_industry == null){
        member_industry = $(this).data('first_name') + ' hasn\'t added an Industry.';
    }
    
    $.ajax({
        url: window.base_url + 'member/mdirectory/get_address',
        type: 'get',
        data: {'braintree_id': member_braintree_customer_id},
        dataType: 'json',
        success: function(response){
            $('.loadingDiv').hide();
            /*
            $data = array(
                'street_address' => '',
                'extended_address' => '',
                'locality' => '',
                'postalcode' => '',
                'countrycode' => ''
            );
            */
            if (response.street_address == '' && response.extended_address == '' && response.locality == '' && response.postalcode == '' && response.countrycode == '') {
                member_address = '';
            } else {
                var address = '';
                member_address = (response.street_address != '') ? response.street_address + ', ':'';
                member_address += member_address + (response.extended_address != '') ? response.extended_address + ', ':'';
                member_address += member_address + (response.locality != '') ? response.locality + ', ':'';
                member_address += member_address + (response.postalcode != '') ? response.postalcode + ', ':'';
                member_address += member_address + (response.countrycode != '') ? response.countrycode:'';
                member_address = '<li class="list-group-item text-muted bg-pink member-address"><i class="fas fa-map-pin"></i><span class="tab small">' + member_address + '</span></li>';
            }
            
            var member_bootbox = bootbox.alert({
                className: 'directory-modal',
                message: '<div class="text-center"><img src="' + member_pic + '" class="img-thumbnail rounded-circle" width="120"><h5>' + member_name + '</h5><h6>' + intro + '</h6></div><hr class="hr-line" style="width:50%" /><div class="text-left"><h6><span class="bot-border">Bio</span></h6><p>' + member_bio + '</p><h6><span class="bot-border">Current Occupation</span></h6><p>' + member_occupation + '</p><h6><span class="bot-border">Company</span></h6><p>' + member_company + '</p><h6><span class="bot-border">Tags</span></h6><p>' + member_industry + '</p><p></p><h6><span class="bot-border">City</span></h6><p>'+member_city+'</p><ul class="list-group list-group-flush text-left member-details">' + member_address + '<li class="list-group-item text-muted bg-pink"><i class="far fa-calendar"></i><span class="tab small">Joined: ' + member_month + ' ' + member_year + '</span></li></ul></div>'
            });
            //<li class="list-group-item text-muted bg-pink"><i class="far fa-envelope"></i><span class="tab small">' + member_email + '</span></li><li class="list-group-item text-muted bg-pink text-center"><button class="btn btn-brunchwork contact-member-form" data-to="' + member_email + '">CONTACT</button></li>
            member_bootbox.init(function(){
                $(this).find('.modal-body').addClass('bg-pink');
                $(this).find('.modal-footer').hide();
            });
        }
    });
});

$(document).on('click', 'button.contact-member-form', function(){
    $('.loadingDiv').hide();
    email_to = $(this).data('to');
    orig_content = $('.directory-modal').find('.bootbox-body').html();
    $('.directory-modal').find('.bootbox-body').empty();
    $('.directory-modal').find('.bootbox-body').load(window.base_url + 'member/mdirectory/contact_member', { email: email_to });
    $('input#dir_to_email').val(email_to);
});

$(document).on('click', 'button.cancel_dir_send_email', function(){
    //$('.directory-modal').modal('hide');
    $('.directory-modal').find('.bootbox-body').empty();
    $('.directory-modal').find('.bootbox-body').html(orig_content);

});

$(document).on('click', 'button.cancel_req_intro_email', function(){
    $('.support-form-modal').modal('hide');
    $('.support-form-modal').find('.bootbox-body').empty();
    $('.support-form-modal').find('.bootbox-body').html(orig_content);

});

$(document).on('click', 'button.clear_dir_email', function(){
    $('#contact-memberdir-form')[0].reset();
});

$(document).on('click', 'button.send_dir_email', function(){
    $('.loadingDiv').show();
    if($('input#dir_to_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#contact-memberdir-form").valid()){
        var contact_member_data = $("#contact-memberdir-form").serializeArray();
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/contact_member', 
            data: contact_member_data,
            dataType : 'JSON',
            success: function (response) {
                $('.loadingDiv').hide();
                $('#contact-memberdir-form')[0].reset();
                $('.directory-modal').find('.bootbox-body').prepend('<div class="twenty-spacer"></div><div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('label.error').css('display', 'none');
    }
});

$(document).on('click', 'button.clear_req_intro_email', function(){
    $('#contact-reqintro-form')[0].reset();
});

$(document).on('click', 'button.send_req_intro_email', function(){
    $('.loadingDiv').show();
    if($('input#dir_to_email').val() == null){
        $('label.error').css('display', 'none');
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid red');
    } else {
        $('span.select2-selection.select2-selection--single.error').css('border', '1px solid #ced4da;')
    }
    if($("#contact-reqintro-form").valid()){
        var contact_member_data = $("#contact-reqintro-form").serializeArray();
        console.log(contact_member_data);
        $.ajax({
            type: "POST", 
            url: window.base_url+'user/request_intro_member', 
            data: contact_member_data,
            dataType : 'JSON',
            success: function (response) {
                $('.loadingDiv').hide();
                $('#contact-reqintro-form')[0].reset();
                $('.support-form-modal').find('.bootbox-body').prepend('<div class="twenty-spacer"></div><div class="alert alert-' + response.alert_type + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + response.message + '</div>');
            },
                error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log("There was an error: " + errorThrown);  
            }
        });
    } else {
        $('label.error').css('display', 'none');
    }
});

$(document).on('click', 'a.request-intro-form', function(){

    $('.loadingDiv').hide();
    email_to = 'concierge@brunchwork.com';

    //email_to = 'iamnirveek@gmail.com';
    requested_person_name = $(this).data('first_name') + " " +$(this).data('last_name');
    
    $('input#dir_to_email').val(email_to);
    var support_contact = bootbox.dialog({
        className: 'support-form-modal',
        message: '<div id="form-holder"></div>'
    });
    support_contact.init(function(){
        $(this).find('.modal-body').addClass('bg-pink');
    });
    $('.support-form-modal').find('div#form-holder').load(window.base_url + 'member/mdirectory/request_intro', { email: email_to, requested_person: requested_person_name  });
    
});